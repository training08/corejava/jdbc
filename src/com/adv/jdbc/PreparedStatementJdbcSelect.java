package com.adv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PreparedStatementJdbcSelect {

	public static void main(String[] args) {
		dynamicVal(5);
	}

	private static void dynamicVal(int idVal) {
		Connection conn = null;
		try {
			// 1. Load Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// 2. Establish Connection
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/training08", "root", "root");
			// 3. Create Statement
			PreparedStatement psStmt = conn.prepareStatement("SELECT id, first_name, email, last_name FROM training08.table_1 where id = ?");
			psStmt.setInt(1, idVal);
			// 4. Execute Query
			ResultSet rs = psStmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String fName = rs.getString("first_name");
				String email = rs.getString("email");
				String lName = rs.getString("last_name");
				System.out.println("id="+id+", fName="+fName+", email="+email+", lName="+lName);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
