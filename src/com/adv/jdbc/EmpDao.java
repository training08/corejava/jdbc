package com.adv.jdbc;

public class EmpDao {

	private int id;
	
	private String fname;
	
	private int age;
	
	private String email;

	public EmpDao(int id, String fname, int age, String email) {
		super();
		this.id = id;
		this.fname = fname;
		this.age = age;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "EmpDao [id=" + id + ", fname=" + fname + ", age=" + age + ", email=" + email + "]";
	}
}
