package com.adv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementJdbcSelectWhere {

	public static void main(String[] args) {
		stmtSelectConcat(4);
	}

	public void m1() {
		throw new RuntimeException();
//		throw new SQLException();
	}
	
	private static void stmtSelectConcat(int idVal) {
		Connection conn = null;
		try {
			// 1. Load Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// 2. Establish Connection
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/training08", "root", "root");
			// 3. Create Statement
			Statement stmt = conn.createStatement();
			// 4. Execute Query
			ResultSet rs = stmt.executeQuery("SELECT id, first_name, email, last_name FROM training08.table_1 where id = " + idVal);
			// 5. iterate the resultset
			String col1 = rs.getMetaData().getColumnName(1);
			String col2 = rs.getMetaData().getColumnName(2);
			String col3 = rs.getMetaData().getColumnName(3);
			String col4 = rs.getMetaData().getColumnName(4);
			System.out.println("col1="+col1+", col2="+col2+", col3="+col3+", col4="+col4);
			while (rs.next()) {
				int id = rs.getInt("id");
				String fName = rs.getString("first_name");
				String email = rs.getString("email");
				String lName = rs.getString("last_name");
				System.out.println("id="+id+", fName="+fName+", email="+email+", lName="+lName);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
