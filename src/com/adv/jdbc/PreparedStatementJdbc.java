package com.adv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementJdbc {

	public static void main(String[] args) {
//		dynamicVal(5, "fox", "f@gmail.com");
		EmpDao[] empDaos = new EmpDao[3];
		empDaos[0] = new EmpDao(1, "A1", 10, "a1@g.com");
		empDaos[1] = new EmpDao(2, "B1", 13, "b1@g.com");
		empDaos[2] = new EmpDao(3, "C1", 12, "c1@g.com");
		dynamicVal(empDaos);
	}

	//private static void dynamicVal(int id, String fName, String email) {
	private static void dynamicVal(EmpDao[] empDaos) {
		Connection conn = null;
		try {
			// 1. Load Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// 2. Establish Connection
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/training11", "root", "root");
			// 3. Create Statement
			PreparedStatement psStmt = conn.prepareStatement("insert into emp (id, name, age, email) values (?, ?, ?, ?)");
			for(EmpDao empDao: empDaos) {
				psStmt.setInt(1, empDao.getId());
				psStmt.setString(2, empDao.getFname());
				psStmt.setInt(3, empDao.getAge());
				psStmt.setString(4, empDao.getEmail());
				psStmt.addBatch();
			}
			// 4. Execute Query
			int[] result = psStmt.executeBatch();
			System.out.println("Result---->"+result.length);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
